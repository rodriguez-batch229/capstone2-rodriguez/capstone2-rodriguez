const express = require("express");
const jwt = require("jsonwebtoken");

const router = express.Router();
const commentController = require("../controllers/commentController");
const auth = require("../auth.js");

// adding comment
router.post("/addComment", (req, res) => {
   const data = req.body;

   commentController.addComment(data).then((resultFromTheControler) => {
      res.send(resultFromTheControler);
   });
});

// retrieve comments
router.get("/", auth.verify, (req, res) => {
   const userData = auth.decode(req.headers.authorization).isAdmin;

   commentController
      .retrieveComments(userData)
      .then((resultFromTheControler) => {
         res.send(resultFromTheControler);
      });
});

// Exports all Routers
module.exports = router;
