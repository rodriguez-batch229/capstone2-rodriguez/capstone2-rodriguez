const express = require("express");
const jwt = require("jsonwebtoken");

const router = express.Router();
const userControllers = require("../controllers/userController");
const productControllers = require("../controllers/productController");
const auth = require("../auth.js");

// register user
router.post("/register", (req, res) => {
   userControllers.register(req.body).then((resultFromController) => {
      res.send(resultFromController);
   });
});
// retrieved user details
router.get("/details", auth.verify, (req, res) => {
   // uses the decode method defined in the auth.js to retrieve user info from request header
   const userData = auth.decode(req.headers.authorization);

   userControllers
      .getProfile({ userId: userData.id })
      .then((resultFromController) => res.send(resultFromController));
});

//Only Admin can access the users info
router.get("/", auth.verify, (req, res) => {
   const userData = auth.decode(req.headers.authorization);
   if (userData.isAdmin) {
      userControllers.getAllUsers(req.body).then((resultFromController) => {
         res.send(resultFromController);
      });
   } else {
      res.send(false);
   }
});

// User Login
router.post("/login", (req, res) => {
   userControllers.loginUser(req.body).then((resultFromController) => {
      res.send(resultFromController);
   });
});

// Setting user as admin by only admin
router.put("/setAdmin", auth.verify, (req, res) => {
   const userData = auth.decode(req.headers.authorization);
   if (userData.isAdmin) {
      let userId = req.body.userId;

      userControllers.setAsAdmin({ userId }).then((resultFromController) => {
         res.send(resultFromController);
      });
   } else {
      res.send(false);
   }
});
// Setting user as admin by only admin
router.put("/setUser", auth.verify, (req, res) => {
   const userData = auth.decode(req.headers.authorization);
   if (userData.isAdmin == true) {
      let userId = req.body.userId;

      userControllers.setAsUser({ userId }).then((resultFromController) => {
         res.send(resultFromController);
      });
   } else {
      res.send(false);
   }
});

//

module.exports = router;
