// modules
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Local modules
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const Cart = require("../models/Cart");
const auth = require("../auth");

// Place order (only log user)
module.exports.placeOrder = (data) => {
   return User.findOne({ username: data.userData }).then((result, error) => {
      if (result == null || result.IsAdmin) {
         return false;
      } else {
         // checking if the product entered is available/valid
         return Product.findOne({
            name: data.order.products,
         }).then((productResult, error) => {
            if (productResult == null) {
               return false;
            } else {
               if (productResult.isActive) {
                  let requestOrder = new Order({
                     userId: result.id,
                     username: result.username,
                     email: result.email,
                     products: {
                        productId: productResult.id,
                        productName: productResult.name,
                        quantity: data.order.quantity,
                        price: productResult.price,
                        subTotal: data.order.quantity * productResult.price,
                     },
                     totalAmount: data.order.quantity * productResult.price,
                  });
                  // if the product is available, order will be save
                  if (productResult) {
                     return requestOrder
                        .save()
                        .then((result, error) => {
                           if (error) {
                              return false;
                           } else {
                              return true;
                           }
                        })
                        .then((result) => {
                           return true;
                        });
                  }
                  // if the product is unavailable/not valid
                  else {
                     return false;
                  }
               } else {
                  return false;
               }
            }
         });
      }
   });
};

// Check all orders  (admin only)
module.exports.checkAllOrders = async (data) => {
   // Check if the log user is admin
   if (data.isAdmin) {
      // finding the orders
      return Order.find({ isActive: true }).then((result, error) => {
         if (result) {
            return result;
         } else {
            return false;
         }
      });
   } else {
      return false;
   }
};

// Check orders for authenticated users (admin only)
module.exports.checkOrderFromAUser = async (data) => {
   if (data.isAdmin) {
      return Order.find({ userId: data.userId }).then((result, error) => {
         if (result.length < 0) {
            return false;
         } else {
            console.log(result.length);
            return result;
         }
      });
   } else {
      return false;
   }
};
// Check orders for authenticated users (user)
module.exports.checkOrderUser = (data) => {
   return Order.find({ username: data.username, isActive: true }).then(
      (result, error) => {
         return result;
      }
   );
};
// Check Cancelled orders for authenticated users (user)
module.exports.checkOrderUserCanceled = (data) => {
   return Order.find({ username: data.username, isActive: false }).then(
      (result, error) => {
         return result;
      }
   );
};

// Cancel orders for authenticated users (user)
module.exports.cancelOrder = (data) => {
   console.log(data.orderId);
   return Order.findById(data.orderId).then((result, error) => {
      console.log(result);
      if (result.length < 0) {
         console.log(result);
         return false;
      } else {
         result.isActive = false;
         return result.save().then((save, error) => {
            if (save) {
               return true;
            } else {
               return false;
            }
         });
         // if (result.length < 0) {
         // }

         // return result.save().then((result, error) => {
         //    if (error) {
         //       return false;
         //    } else {
         //       return true;
         //    }
         // });
      }
   });
};
