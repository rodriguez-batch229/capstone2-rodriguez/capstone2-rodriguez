const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Local modules
const Comment = require("../models/Comment");

module.exports.addComment = (data) => {
   if (data !== null) {
      let comment = new Comment({
         username: data.username,
         email: data.email,
         comment: data.comment,
         rating: data.rating,
      });

      return comment.save().then((result, error) => {
         if (result) {
            return true;
         } else {
            return false;
         }
      });
   } else {
      return false;
   }
};

module.exports.retrieveComments = (data) => {
   if (data) {
      return Comment.find().then((results, error) => {
         if (results) {
            return results;
         } else {
            return false;
         }
      });
   } else {
      return false;
   }
};
