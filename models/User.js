const mongoose = require("mongoose");

const month = [
   "January",
   "February",
   "March",
   "April",
   "May",
   "June",
   "July",
   "August",
   "September",
   "October",
   "November",
   "December",
];

const userSchema = new mongoose.Schema({
   username: {
      type: String,
      required: [true, "Username is required"],
   },
   email: {
      type: String,
      required: [true, "Email is required"],
   },
   password: {
      type: String,
      required: [true, "Password is required"],
   },
   isAdmin: {
      type: Boolean,
      default: false,
   },
   createdOn: {
      type: String,
      default: `${new Date().getFullYear()} ${
         month[new Date().getMonth()]
      } ${new Date().getDate()} `,
   },
});

module.exports = mongoose.model("User", userSchema);
