const mongoose = require("mongoose");

const commentSchema = mongoose.Schema({
   username: {
      type: String,
      required: true,
   },
   email: {
      type: String,
      required: true,
   },
   comment: {
      type: String,
      required: true,
   },
   rating: {
      type: Number,
      required: true,
   },
});

module.exports = mongoose.model("Comment", commentSchema);
