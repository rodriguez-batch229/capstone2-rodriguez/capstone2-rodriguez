// Modules
const express = require("express");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const cors = require("cors");
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");
const cartRoutes = require("./routes/cart");
const commentsRoutes = require("./routes/comment");

const app = express();
// Routes

// Database Connection
mongoose.connect(
   "mongodb+srv://admin:admin1234@cluster0.tt3qfyy.mongodb.net/Capstone2?retryWrites=true&w=majority",
   {
      useNewUrlParser: true,
      useUnifiedTopology: true,
   }
);
// Setting notifications if the database is connected
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("We are now connected to the database"));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/orders/cart", cartRoutes);
app.use("/comments", commentsRoutes);

// Middlewares for routes

// Server listening
app.listen(process.env.PORT || 4000, () => {
   console.log(`API is now connected on port: ${process.env.PORT}`);
});
